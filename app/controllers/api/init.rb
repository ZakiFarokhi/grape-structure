require 'grape-swagger'

module Api
    class Init < Grape::API
        mount Api::V1::Main
        add_swagger_documentation
    end
end