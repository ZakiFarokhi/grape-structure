Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'application#index'  
  mount Api::Init, at:"/web/"
  mount GrapeSwaggerRails::Engine => 'web/doc/v1'
end
